## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Vue Trivia App!

* Worked with Jaemin Park on this project, at https://gitlab.com/jpark0624.

* Used https://opentdb.com/api_config.php to generate an API link for the questions.

* Number of questions: 10

* Category: Science: Mathematics

* Includes: 

    1. Start screen.

    2. One question at a time from the API.

    3. Multiple choice questions with four buttons or two for True/False.

    4. Automatically moves on to the next question, displays result screen once all have been answered.

    5. Result screen contains both correct and user chosen answers.

    6. Each question scored 10 points, display score.

