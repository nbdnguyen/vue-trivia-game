import Vuex from "vuex";
import Vue from "vue";
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        points: 0,
        correctAnswers: 0,
        incorrectAnswers: 0,
        questions: []
    },

    mutations: {
        answerCorrectly(state) {
            state.points += 10;
            state.correctAnswers++;
        },
        answerIncorrectly(state) {
            state.incorrectAnswers++;
        },
        addQuestion(state, q) {
            state.questions.push(...q);
        }
    }
})