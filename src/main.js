import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import StartPage from './components/StartPage'
import Questions from './components/QuestionList'
import ResultPage from './components/ResultPage'
import store from "./store/store"

Vue.use(VueRouter);

const routes = [
  { path: '/', component: StartPage},
  { path: '/questions', component: Questions},
  { path: '/result', component: ResultPage}
];

const router = new VueRouter({routes})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

document.body.style.backgroundColor = "#FFDAFC";