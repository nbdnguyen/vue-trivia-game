const QUESTION_URL = 'https://opentdb.com/api.php?amount=10&category=19';

let answers = [];
export const getQuestions = () => {
    return fetch(QUESTION_URL)
    .then(response => response.json())
    .then(response => response.results);
}

export const addAnswers = (ans) => {
    answers = ans;
  }

export const getAnswers = () => {
    const allAnswers = answers;
    return allAnswers;
}
